import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from "@angular/router/testing";

import { MarkdownModule } from 'ngx-markdown';
import { ContentComponent } from './content.component';

describe('ContentComponent', () => {
	let component: ContentComponent;
	let fixture: ComponentFixture<ContentComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
		declarations: [
			ContentComponent
		],
		imports: [
			RouterTestingModule,
			HttpClientModule,
			MarkdownModule.forRoot()
		]
		})
		.compileComponents();

		fixture = TestBed.createComponent(ContentComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
