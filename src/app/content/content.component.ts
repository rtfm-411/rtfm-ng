import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MarkdownService } from 'ngx-markdown';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { MdFetchService } from '../md-fetch.service';
import { Content } from '../data/content';

import * as defaultContent from '../../assets/content/default.json';

@Component({
	selector: 'app-content',
	templateUrl: './content.component.html',
	styleUrls: ['./content.component.scss']
})

export class ContentComponent implements OnInit {
	markdown: Content = defaultContent;
	isMarkdownReady: boolean = false;

	constructor(
		private MdFetchService: MdFetchService,
		private route: ActivatedRoute
	) { }

	ngOnInit(): void {
		const base: string = this.route.snapshot.pathFromRoot[1]?.url[0]?.path;
		const page: string = this.route.snapshot.paramMap.get('page')!;
		const currentLocator = this.markdown.locator;
		let newLocator = currentLocator;
		if (base) {
			newLocator = base || currentLocator
			if (page) {
				newLocator = `${newLocator}/${page}`;
			}
		}

		if ( newLocator !== currentLocator ) {
			this.getContent(newLocator);
		}
		else {
			this.isMarkdownReady = true;
		}
	}

	getContent(locator: string): void {
		this.MdFetchService.getContent(locator)
		.subscribe(
			(content) => {
				/*
				*	horrible workaround to avoid writing a class for the interface
				*	Object.assign(this.markdown, content) will complain because
				*	there is no setter
				*/
				const updatedContent = structuredClone(this.markdown);
				Object.assign(updatedContent, {locator}, content);
				this.markdown = updatedContent;
				this.isMarkdownReady = true;
			}
		);
	}
}
