import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ContentComponent } from './content/content.component';

const routes: Routes = [
	{
		path: '',
		component: ContentComponent,
		title: 'RTFM-411'
	},
	{
		path: 'about',
		title: 'RTFM-411: about',
		component: ContentComponent
	},
	{
		path: 'tech/:page',
		title: 'RTFM-411: tech',
		component: ContentComponent
	},
	{
		path: 'tech',
		title: 'RTFM-411: tech',
		component: ContentComponent
	},
	{
		path: '**',
		title: 'RTFM-411: Nothing Here',
		component: ContentComponent
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, {
		anchorScrolling: 'enabled',
		scrollOffset: [0, 64],
		scrollPositionRestoration: 'enabled'
	})],
	exports: [RouterModule]
})

export class AppRoutingModule { }
