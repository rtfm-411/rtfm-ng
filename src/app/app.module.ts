import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import {
	MarkdownModule,
	MarkedOptions,
	MarkedRenderer
} from 'ngx-markdown';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuComponent } from './menu/menu.component';
import { ContentComponent } from './content/content.component';
import { AnchorService } from './services/anchor.service';

export function markedOptionsFactory(anchorService: AnchorService): MarkedOptions {
	const renderer = new MarkedRenderer();
	renderer.link = (href: string, title: string, text: string) => {
		return MarkedRenderer.prototype.link.call(renderer, anchorService.normalizeExternalUrl(href), title, text);
	}

	return { renderer };
}

@NgModule({
	declarations: [
		AppComponent,
		MenuComponent,
		ContentComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		MatMenuModule,
		MatButtonModule,
		HttpClientModule,
		MarkdownModule.forRoot({
			loader: HttpClient,
			markedOptions: {
				provide: MarkedOptions,
				useFactory: markedOptionsFactory,
				deps: [AnchorService]
			}
		})
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
