import { Content } from '../content';

const mapper: any = {
	id: 'id',
	content: 'content',
	locator: 'locator'
};

export function adaptContent(json: any): Content {
	const adaptedContent: any = {};
	const mappedAttributes: Array<string> = Object.keys(mapper);

	for ( let attribute of mappedAttributes ) {
		const locator: string = mapper[attribute];
		adaptedContent[attribute] = json[locator];
	}

	return adaptedContent;
}
