export interface Content {
	id: string,
	content: string,
	locator: string
}
