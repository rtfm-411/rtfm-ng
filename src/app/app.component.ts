import { Component, HostListener } from '@angular/core';

import { AnchorService } from './services/anchor.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})

export class AppComponent {
	title = 'rtfm-411';

	@HostListener('document:click', ['$event'])
	onDocumentClick(event: Event) {
		this.anchorService.interceptClick(event);
	}

	constructor(
		private anchorService: AnchorService
	) { }
}
