import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { MdFetchService } from './md-fetch.service';

describe('MdFetchService', () => {
	let service: MdFetchService;

	beforeEach(() => {
		TestBed.configureTestingModule({
		imports: [
			HttpClientModule
		]
		});
		service = TestBed.inject(MdFetchService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});
