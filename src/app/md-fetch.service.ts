import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

import { environment } from '../environments/environments';
import { Content } from './data/content';
import { adaptContent } from './data/helpers/content.adapter';

@Injectable({
	providedIn: 'root'
})

export class MdFetchService {

	httpOptions = {
		headers: new HttpHeaders()
	};
		//responseType: 'text'

	constructor( private http: HttpClient ) {
	}

	//getContent(): Observable<Content> {
	getContent( locator:string ): Observable<Content> {
		const path = environment.docs;

		return this.http.get<HttpResponse<Content>>(
			`${path}/${locator}`,
			this.httpOptions
		)
		.pipe(
			map((json: any) => adaptContent(json)),
			catchError(this.handleError<any>('getContent'))
		);
	}

	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			let errorObj = {
				content: '# Fail\n\nAn unknown error occurred.'
			};
			if (error?.error?.content) {
				errorObj = error.error;
			}
			else if (error.message){
				errorObj.content = `# Fail\n\n${error.message}`;
			}
			return of(errorObj as T);
		};
	}
}
